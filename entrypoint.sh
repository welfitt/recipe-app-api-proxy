#!/bin/sh
set -e
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
# This runs nginx in the foreground 
nginx -g 'daemon off;'